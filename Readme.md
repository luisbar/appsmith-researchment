## How to run?

1. Install docker
2. Create a `docker.env` file taking as reference the `docker.env.template` and set your database user and password
3. Create a `encryption.env` file taking as reference the `encryption.env.template` and set the following values in order to run this example
    ```
    APPSMITH_ENCRYPTION_PASSWORD=mF7zSXCMDQoTw
    APPSMITH_ENCRYPTION_SALT=yPlpOc4vY5Q89
    ```
4. Run `docker-compose up -d`
5. Open the following url `http://localhost`
6. Login using the following credentials
    ```
    luisbar180492@gmail.com
    huachinango123
    ```